let container;
let canvas;
let headerHeight = 13;

let videoHeight = 240;
let videoWidth = 320;
let videoDisplayWidth;
let videoDisplayHeigh;
let videoVerticalOffset;
let selfie;
let selfieImg;

let posenet;
let loadingPosenet = true;
let poses = [];
let mobilenet;
let loadingMobielnet = true;

let mode = -1; // loading screen
let isHuman = false;

let selfieBtn;
let backBtn;
let continueBtn;
let resultDiv;
let output;
let confidence;
let info;
let explanation;
let toggle = true;
let questionmark;


// Models ready
function mobilenetReady() {
  loadingMobielnet = false;
  if (!loadingPosenet && !loadingMobielnet) {
    mode == 0;
    welcomeScreen();
  }
}


function posenetReady(error, results) {
  loadingPosenet = false;
  if (!loadingPosenet && !loadingMobielnet) {
    mode == 0;
    welcomeScreen();
  }
}


function setup() {
  container = select('#sketch-holder');
  canvas = createCanvas(container.width, container.height);
  canvas.style('z-index', '-1');
  canvas.parent('sketch-holder');

  // webcam
  video = createCapture(VIDEO);
  video.size(videoWidth, videoHeight);
  video.hide();
  videoDisplayWidth = container.width;
  videoDisplayHeight = (container.width / 4) * 3;
  videoVerticalOffset = (container.height / 100) * headerHeight;

  // AI
  mobilenet = ml5.imageClassifier('MobileNet', mobilenetReady);
  let options = {
    imageScaleFactor: 0.3,
    outputStride: 16,
    flipHorizontal: false,
    minConfidence: 0.5,
    maxPoseDetections: 1,
    scoreThreshold: 0.5,
    nmsRadius: 20,
    detectionType: 'single',
    multiplier: 0.75
  }
  posenet = ml5.poseNet(posenetReady, options);
  posenet.on('pose', function (results) {
    poses = results;
    mobilenet.classify(selfieImg, gotResults);
  });

  // Interaction and GUI
  selfieBtn = select('#selfieButton');
  selfieBtn.mousePressed(takeSelfie);
  backBtn = select('#backButton');
  continueBtn = select('#continueButton');
  resultDiv = select('#result');
  output = select('#output');
  confidence = select('#confidence');
  output.html("Please wait...");
  confidence.html("");
  info = select('#info');
  explanation = select('#explanation');
  info.mousePressed(toggleInfo);
  questionmark = select("#questionmark");
  let fontS = "" + (videoDisplayWidth / 45) + "px";
  explanation.style('font-size', fontS);
}

function toggleInfo() {
  if (toggle) {
    explanation.removeClass("invisible");
    toggle = !toggle;
    questionmark.html("X");
  } else {
    explanation.addClass("invisible");
    toggle = !toggle;
    questionmark.html("?");

  }
}

function draw() {
  if (mode == -1) {
    loadingScreen();
  }
  // if user is on welcome screen
  if (mode == 0) {
    var frame = video.get();
    push();
    translate(videoDisplayWidth, 0);
    scale(-1.0, 1.0);
    image(frame, 0, videoVerticalOffset, videoDisplayWidth, videoDisplayHeight);
    pop();
  }
  // if selfie has been taken
  else if (mode == 1) {
    image(selfie, 0, videoVerticalOffset, videoDisplayWidth, videoDisplayHeight);
  } else if (mode == 2) {
    if (isHuman) {
      image(selfie, 0, videoVerticalOffset, videoDisplayWidth, videoDisplayHeight);
      // drawFaceRect();
    }
  }
}

function windowResized() {
  container = select('#sketch-holder');
  resizeCanvas(container.width, container.height);
  videoDisplayWidth = container.width;
  videoDisplayHeight = (container.width / 4) * 3;
  videoVerticalOffset = (container.height / 100) * headerHeight;
  let fontMaxSize = videoDisplayWidth / 30;
  fitty('#output', {
    minSize: 3,
    maxSize: fontMaxSize,
    multiLine: false,
  });
  let fontS = "" + (videoDisplayWidth / 45) + "px";
  explanation.style('font-size', fontS);
}


function takeSelfie() {
  selfie = video.get();
  mode = 1; // selfie has been taken
  selfieScreen();
}


function loadingScreen() {
  mode = -1; // models have not been loaded...
  continueBtn.addClass("invisible");
  backBtn.addClass("invisible");
  output.html("Please wait...");
  confidence.html("Loading AI models for image analysis.");
}


function welcomeScreen() {
  mode = 0; // back to the beginning
  selfieBtn.removeClass("invisible");
  continueBtn.addClass("invisible");
  backBtn.addClass("invisible");
  resultDiv.addClass("invisible");
  output.html("Analyzing...");
  confidence.html("");
}


function selfieScreen() {
  selfieBtn.addClass("invisible");
  continueBtn.html("Classify selfie &#9654;");

  continueBtn.removeClass("invisible");
  backBtn.removeClass("invisible");
  backBtn.mousePressed(welcomeScreen);
  continueBtn.mousePressed(resultScreen);
}


function resultScreen() {
  selfieBtn.addClass("invisible");
  backBtn.addClass("invisible");
  continueBtn.addClass("invisible");
  classify();
  continueBtn.addClass("invisible");
  resultDiv.removeClass("invisible");
  let fontMaxSize = videoDisplayWidth / 30;
  fitty('#output', {
    minSize: 3,
    maxSize: fontMaxSize,
    multiLine: false,
  });
}


function classify() {
  selfieImg = createImg(selfie.canvas.toDataURL(), () => {
    poses = posenet.singlePose(selfieImg);
  });
  selfieImg.hide();
}


function gotResults(error, results) {
  let label; // human or something else
  // let prob;
  mode = 2;
  if (error) {
    console.error(error);
  } else {
    let objectness = Math.round(100 * results[0].confidence);
    let humanness = faceScore() - 30; // we subtract 30 to make it easier to fool the AI
    selfieImg.remove();

    if (humanness >= objectness) {
      label = "Human";
      // prob = Math.round(humanness);
      isHuman = true;
    } else {
      label = results[0].label;
      // prob = objectness;
      isHuman = false;
    }
    output.html("You are a: " + label + "!");
    // confidence.html("I am " + prob + "% sure.");
    confidence.html("Please report the result in our questionnaire.")
  }
}


function faceScore() {
  let s = 0;
  for (let i = 0; i < 5; i++) {
    s = s + poses[0].pose.keypoints[i].score;
  }
  return Math.round(100 * (s / 5.0));
}


function drawFaceRect() {
  if (poses.length) {
    noFill();
    stroke(241, 17, 97);
    strokeWeight(6);

    let noseX = (poses[0].pose.keypoints[0].position.x) / 320 * videoDisplayWidth;
    let leftEyeX = (poses[0].pose.keypoints[1].position.x) / 320 * videoDisplayWidth;
    let rightEyeX = (poses[0].pose.keypoints[2].position.x) / 320 * videoDisplayWidth;
    let leftEarX = (poses[0].pose.keypoints[3].position.x) / 320 * videoDisplayWidth;
    let rightEarX = (poses[0].pose.keypoints[4].position.x) / 320 * videoDisplayWidth;
    let noseY = (poses[0].pose.keypoints[0].position.y) / 240 * videoDisplayHeight;
    let leftEyeY = (poses[0].pose.keypoints[1].position.y) / 240 * videoDisplayHeight;
    let rightEyeY = (poses[0].pose.keypoints[2].position.y) / 240 * videoDisplayHeight;
    let leftEarY = (poses[0].pose.keypoints[3].position.y) / 240 * videoDisplayHeight;
    let rightEarY = (poses[0].pose.keypoints[4].position.y) / 240 * videoDisplayHeight;

    let rightX = Math.max(noseX, leftEyeX, leftEarX);
    let leftX = Math.min(noseX, rightEyeX, rightEarX);
    let highY = Math.min(noseY, leftEyeY, leftEarY, rightEyeY, rightEarY);
    let faceWidth = rightX - leftX;
    let vOffset = faceWidth / 8;
    let hOffset = faceWidth / 1.5;
    let faceHeight = 1.5 * faceWidth;
    rect(leftX - vOffset, highY - hOffset + videoVerticalOffset, faceWidth + 2 * vOffset, faceHeight);
  }
}